<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Home extends CI_Controller
{
    public $data;

    public function __contruct()
    {
        parent::__contruct();
        $this->load->model('CursosModel');
    }

    public function index()
    {
        $this->data = array(
            'javascript' => array('echo.min', 'image-load.min'),
        );
        $this->template->render('site/index', $this->data);
    }

    public function cursos()
    {
        $this->data = array(
            'titulo' => 'Lista de cursos',
            'model' => '',
        );

        $this->template->render('site/lista-cursos', $this->data);
    }

    public function sobre()
    {
        $this->data = array(
            'titulo' => 'Sobre o site',
        );

        $this->template->render('site/sobre', $this->data);
    }

    public function contato()
    {
        $this->data = array(
            'titulo' => 'Fale conosco',
            'nome' => '',
            'email' => '',
            'curso' => $this->dropdownCursos(),
            'mensagem' => '',
            'consultoria' => $this->dropdownConsultoria(),
        );

        $this->template->render('site/contato', $this->data);
    }

    private function dropdownConsultoria()
    {
        $options = array(
            'não' => 'Não',
            'sim' => 'Sim',
        );

        return $options;
    }

    private function dropdownCursos()
    {
        $options = array(
            '' => '',
            'PHP Básico' => 'PHP Básico',
        );

        return $options;
    }
}
