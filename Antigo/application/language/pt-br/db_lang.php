<?php

$lang['db_invalid_connection_str'] = 'Não foi possível acessar o banco de dados com essa string de conexão.';
$lang['db_unable_to_connect'] = 'Não foi possível acessar o banco de dados com os parâmetros informados.';
$lang['db_unable_to_select'] = 'Não foi pssível acessar o banco de dados: %s';
$lang['db_unable_to_create'] = 'Não foi possível criar o banco de dados: %s';
$lang['db_invalid_query'] = 'A consulta realizada não é válida.';
$lang['db_must_set_table'] = 'Informe a tabela que seve ser usada na consulta.';
$lang['db_must_use_set'] = 'Use o método "set" para atualizar uma entrada.';
$lang['db_must_use_index'] = 'É preciso especificar um índice para realizar "batch updates".';
$lang['db_batch_missing_index'] = 'One or more rows submitted for batch updating is missing the specified index.';
$lang['db_must_use_where'] = '"Updates" sem "where" não são permitidos.';
$lang['db_del_must_use_where'] = '"Deletes" sem "where" ou "like" não são permitidos';
$lang['db_field_param_missing'] = 'To fetch fields requires the name of the table as a parameter.';
$lang['db_unsupported_function'] = 'This feature is not available for the database you are using.';
$lang['db_transaction_failure'] = 'Transação falhou. Rollback executado.';
$lang['db_unable_to_drop'] = 'Não foi possível eliminar o banco de dados.';
$lang['db_unsuported_feature'] = 'Unsupported feature of the database platform you are using.';
$lang['db_unsuported_compression'] = 'The file compression format you chose is not supported by your server.';
$lang['db_filepath_error'] = 'Unable to write data to the file path you have submitted.';
$lang['db_invalid_cache_path'] = 'The cache path you submitted is not valid or writable.';
$lang['db_table_name_required'] = 'Um nome de tabela é necessário nessa operação.';
$lang['db_column_name_required'] = 'Um nome de coluna é necessário nessa operação.';
$lang['db_column_definition_required'] = 'A column definition is required for that operation.';
$lang['db_unable_to_set_charset'] = 'Unable to set client connection character set: %s';
$lang['db_error_heading'] = 'Erro de acesso ao banco de dados.';

/* End of file db_lang.php */
/* Location: ./system/language/english/db_lang.php */