<?php

$lang['required']           = "O campo <strong>%s</strong> é obrigatório.";
$lang['isset']              = "O campo <strong>%s</strong> deve ser preenchido.";
$lang['valid_email']        = "O campo <strong>%s</strong> deve conter um endereço de e-mail válido.";
$lang['valid_emails']       = "O campo <strong>%s</strong> deve conter endereços de e-mail válidos.";
$lang['valid_url']          = "O campo <strong>%s</strong> deve conter uma URL válida.";
$lang['valid_ip']           = "O campo <strong>%s</strong> deve conter um IP válido.";
$lang['min_length']         = "O campo <strong>%s</strong> deve conter pelo menos <strong>%s</strong> caracteres.";
$lang['max_length']         = "O campo <strong>%s</strong> deve conter no máximo <strong>%s</strong> caracteres.";
$lang['exact_length']       = "O campo <strong>%s</strong> deve conter exatamente <strong>%s</strong> caracteres.";
$lang['alpha']              = "O campo <strong>%s</strong> aceita somente letras.";
$lang['alpha_numeric']      = "O campo <strong>%s</strong> aceita somente letras ou números.";
$lang['alpha_dash']         = "O campo <strong>%s</strong> aceita somente letras, números, underscores e hífens.";
$lang['numeric']            = "O campo <strong>%s</strong> aceita somente números.";
$lang['is_numeric']         = "O campo <strong>%s</strong> aceita somente números.";
$lang['integer']            = "O campo <strong>%s</strong> aceita somente números inteiros.";
$lang['regex_match']        = "O campo <strong>%s</strong> não está formatado corretamente.";
$lang['matches']            = "O campo <strong>%s</strong> não está igual ao campo <strong>%s</strong>.";
$lang['not_matches']        = "O campo <strong>%s</strong> precisa ser diferente do campo <strong>%s</strong>.";
$lang['is_unique']          = "<strong>%s</strong> já cadastrado(a).";
$lang['is_natural']         = "O campo <strong>%s</strong> aceita somente núeros positivos.";
$lang['is_natural_no_zero'] = "O campo <strong>%s</strong> aceita somente números maiores que zero.";
$lang['decimal']            = "O campo <strong>%s</strong> só aceita números decimais.";
$lang['less_than']          = "O campo <strong>%s</strong> deve conter um número menor que <strong>%s</strong>.";
$lang['greater_than']       = "O campo <strong>%s</strong> deve conter um número maior que <strong>%s</strong>.";
$lang['valid_cnpj']         = "O campo <strong>%s</strong> não é um CNPJ válido.";
$lang['valid_cpf']          = "O campo <strong>%s</strong> não é um CPF válido.";
$lang['valid_date']         = "O campo <strong>%s</strong> não é uma data (dd/mm/yyyy) válida.";
$lang['valid_date_is_after_today'] = "O campo <strong>%s</strong> deve ser maior que a data de hoje.";
$lang['required_if'] = "O campo <strong>%s</strong> Teste.";


/* End of file form_validation_lang.php */
/* Location: ./system/language/english/form_validation_lang.php */