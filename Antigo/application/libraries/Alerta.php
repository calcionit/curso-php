<?php
defined('BASEPATH') or exit('No direct script access allowed');

/**
 * Description of Alerta
 * Alertas do site. o template está inserido dentro do _templates/header.php, sendo o arquivo _templates/alerta.php
 * @author leandro.trocado
 */
class Alerta
{
    public $CI;

    public function __construct()
    {
        $this->CI = &get_instance();
    }

    public function mensagem($tipo = 'warning', $valor = 'mensagem de alerta')
    {
        $mensagem['tipo'] = $tipo;
        $mensagem['valor'] = $valor;
        
        $this->CI->session->set_flashdata('alerta', $mensagem);
    }
}
