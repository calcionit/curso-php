<?php
defined('BASEPATH') or exit('No direct script access allowed');

/**
 * Description of MY_Form_validation
 *
 * @author marcelo.menezes
 */
class MyFormValidation extends CI_Form_validation
{
    public function __construct()
    {
        parent::__construct();
    }
    
    /**
    * Verifica se a data informada está na estrutura correta do calendário
    * gregoriano.
    *
    * @param String $date Uma string contendo a data no formato dd/mm/yyyy
    * @return boolean
    */
    public function valid_date($date)
    {
        if ($date == "" || is_null($date)) {
            return true;
        }
        
        if (preg_match("/^([0-9]{1,2})\/([0-9]{1,2})\/([0-9]{4})$/", $date, $arr)) {
            $arr = explode("/", $date);

            $dd = $arr[0];
            $mm = $arr[1];
            $yyyy = $arr[2];

            return (checkdate($mm, $dd, $yyyy));
        } else {
            return false;
        }
    }
    
    /**
    * Verifica se a data informada é maior que a data de hoje.
    *
    * @param String $date Uma string contendo a data no formato dd/mm/yyyy
    * @return boolean
    */
    public function valid_date_is_after_today($date)
    {
        if (preg_match("/^([0-9]{1,2})\/([0-9]{1,2})\/([0-9]{4})$/", $date, $arr)) {
            $date1 = $arr[3].$arr[2].$arr[1];
            $today = date("Ymd");
            
            if ($date1 > $today) {
                return true;
            } else {
                return false;
            }
        } else {
            return false;
        }
    }

    /**
     * No match one field to another
     *
     * @access public
     * @param  string
     * @param  field
     * @return bool
     */
    public function not_matches($str, $field)
    {
        if (!isset($_POST[$field])) {
            return false;
        }

        $field = $_POST[$field];

        return ($str === $field) ? false : true;
    }
    
    public function valid_cnpj($str)
    {
        if (strlen($str) <> 18) {
            return false;
        }
        
        $soma1 = ($str[0] * 5) +
            ($str[1] * 4) +
            ($str[3] * 3) +
            ($str[4] * 2) +
            ($str[5] * 9) +
            ($str[7] * 8) +
            ($str[8] * 7) +
            ($str[9] * 6) +
            ($str[11] * 5) +
            ($str[12] * 4) +
            ($str[13] * 3) +
            ($str[14] * 2);
        
        $resto = $soma1 % 11;
        $digito1 = $resto < 2 ? 0 : 11 - $resto;
        
        $soma2 = ($str[0] * 6) +
            ($str[1] * 5) +
            ($str[3] * 4) +
            ($str[4] * 3) +
            ($str[5] * 2) +
            ($str[7] * 9) +
            ($str[8] * 8) +
            ($str[9] * 7) +
            ($str[11] * 6) +
            ($str[12] * 5) +
            ($str[13] * 4) +
            ($str[14] * 3) +
            ($str[16] * 2);
            $resto = $soma2 % 11;
            
        $digito2 = $resto < 2 ? 0 : 11 - $resto;
        
        return (($str[16] == $digito1) && ($str[17] == $digito2));
    }
}
