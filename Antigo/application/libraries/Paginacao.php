<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Paginacao
{
    public $CI;

    public function __construct()
    {
        $this->CI = &get_instance();
        $this->CI->load->library('pagination');
    }
    
    public function criar($totalRows, $baseUrl, $perPage = 15, $numLinks = 5)
    {
        $paginacao['total_rows'] = $totalRows;
        
        $paginacao['baseUrl'] = site_url($baseUrl);
        $paginacao['perPage'] = $perPage;
        $paginacao['num_links'] = $numLinks;

        $paginacao['full_tag_open'] = '<br /><div class="pagination pagination-centered"><ul>';
        $paginacao['full_tag_close'] = '</ul></div>';

        $paginacao['first_link'] = 'Primeira';
        $paginacao['first_tag_open'] = '<li>';
        $paginacao['first_tag_close'] = '<li>';

        $paginacao['last_link'] = 'Ultima';
        $paginacao['last_tag_open'] = '<li>';
        $paginacao['last_tag_close'] = '</li>';

        $paginacao['next_link'] = 'Próximo';
        $paginacao['next_tag_open'] = '<li>';
        $paginacao['next_tag_close'] = '</li>';

        $paginacao['prev_link'] = 'Anterior';
        $paginacao['prev_tag_open'] = '<li>';
        $paginacao['prev_tag_close'] = '</li>';

        $paginacao['cur_tag_open'] = '<li class="active"><a href="#">';
        $paginacao['cur_tag_close'] = '</a></li>';

        $paginacao['num_tag_open'] = '<li>';
        $paginacao['num_tag_close'] = '</li>';

        $this->CI->pagination->initialize($paginacao);
        $html = $this->CI->pagination->create_links();
            
        return $html;
    }
}
