<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Template
{
    public $CI;

    public function __construct()
    {
        $this->CI = &get_instance();
    }

    public function render($view, $data = array())
    {
        //Carrega views do template
        $this->CI->load->view('_templates/header', $data);
        $this->CI->load->view($view);
        $this->CI->load->view('_templates/footer');
    }
}
