<?php
defined('BASEPATH') or exit('No direct script access allowed');

/**
* Cursos
*/
class Cursos extends CI_Model
{
    private $table = 'cursos';
    private $data = array();

    public function __construct()
    {
        parent::__construct();
    }

    /**
    * Getters and Setters
    */

    public function getData()
    {
        retrun $this->data;
    }

    public function setData($data)
    {
        $this->data = $data;
    }

    /*
    * Lista todos os cursos disponíveis na tabela cursos
    */
    public function getAllCursos()
    {

    }

    /*
    * Lista os cursos disponíveis na tabela cursos
    * pelo id_curso
    */
    public function getCursoById($id)
    {

    }
}
