<?php 
if ($this->session->flashdata('alerta')) : 
    $mensagem = $this->session->flashdata('alerta');
?>
    <div class="alert alert-<?php echo $mensagem['tipo'] ?>">
        <a class="close" data-dismiss="alert">×</a>
        <?php echo $mensagem['valor']; ?>
    </div>
<?php endif; ?>
