
        </div>

        <footer class="footer">
            <div class="container">
                <p class="text-center">
                    Copyright © 2015 - <?php echo date("Y"); ?>. Desenvolvido com 
                    <?= anchor('http://codeigniter.com', 'CodeIgniter 3.0', 'target="_new"'); ?>, HTML 5 e 
                    <?= anchor('http://getbootstrap.com/', 'Twitter Bootstrap', 'target="_new"'); ?>!
                </p>
            </div>
        </footer>
        
        <!-- <div class="dmtop" style="bottom: 25px;">Scroll to Top</div> -->

        <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.4/js/bootstrap.min.js"></script>
        
        <?php
        //Carrega scripts adicionais passados pelo Controller
        if (isset($javascript)) :
            foreach ($javascript as $js) :
                //if (file_exists($_SERVER['DOCUMENT_ROOT'].'/curso-php/assets/js/'.$js.'.js')) :
        ?>
                    <script src="<?php echo base_url('assets/js/'.$js.'.js');?>"></script>

        <?php
                //endif;
            endforeach;
        endif;
        ?>
    </body>
</html>
