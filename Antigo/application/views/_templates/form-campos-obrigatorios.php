    <div class="alert alert-block">
        <strong><i class="icon-exclamation-sign"></i> Atenção: 
        </strong> Os campos com <i class="text-error icon-asterisk"></i> são de preenchimento obrigatório.
    </div>
