<?php
defined('BASEPATH') or exit('No direct script access allowed');

?>
<!DOCTYPE html>
<html lang="pt-br">
<head>
	<meta charset="UTF-8">
    <title><?= SITE_TITLE; ?></title>
    <meta name="description" content="<?= DESCRIPTION; ?>">
    <meta name="author" content="<?= AUTHOR; ?>">
    <meta name="keywords" content="<?= KEYWORDS; ?>">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0" />
    <link rel="icon" type="image/ico" href="<?= base_url('assets/images/site/php.ico'); ?>"/>

    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.4/css/bootstrap.min.css">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.4/css/bootstrap-theme.min.css">
    <link rel="stylesheet" href="<?= base_url('assets/css/justified-nav.css'); ?>">

    <?php
    //Carrega os CSSs adicionais passados pelo Controller
    if (isset($css)) :
        foreach ($css as $style) :
            if (file_exists($_SERVER['DOCUMENT_ROOT'].'curso-php/assets/css/'.$style.'.css')) :
    ?>
                <link rel="stylesheet" href="<?= base_url('assets/css/'.$style.'.css') ;?>">
    <?php
            endif;
        endforeach;
    endif;
    ?>
</head>
    <body>
        <div class="wrap container">

        <!-- The justified navigation menu is meant for single line per list item.
           Multiple lines will require custom code not provided by Bootstrap. -->
        <header class="masthead">
            <h3 class="text-muted"><?= SITE_TITLE; ?></h3>
            <nav>
                <ul class="nav nav-justified">
                    <li class="active"><?= anchor('index.php/home', 'Home'); ?></li>
                    <li><?= anchor('index.php/home/cursos', 'Cursos'); ?></li>
                    <li><?= anchor('index.php/home/sobre', 'Sobre'); ?></li>
                    <li><?= anchor('index.php/home/contato', 'Contato'); ?></li>
                </ul>
            </nav>
        </header>

        <?php
        //Exibições de alertas do sistema
        //$this->load->view('_templates/alerta');
        ?>
