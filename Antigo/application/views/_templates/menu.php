<?php $menuAtivo = $this->uri->segment(1); ?>

<div id="menuPrincipal" class="accordion">
    <?php $urlMenu='home'; $tituloMenu='Home';?>
    <div class="accordion-group">
        <div class="accordion-heading">
            <a href="<?php echo site_url($urlMenu)?>" data-parent="#menuPrincipal" data-toggle="false" class="accordion-toggle">
                <i class="icon-home"></i> <?php echo $tituloMenu;?>
            </a>
        </div>
    </div>
    
    <?php if($this->sessao->temPermissao(76,'ver')):?>
        <?php 
        $urlMenuAtivo='ativo'; $tituloMenuAtivo='Ativos por tipo';
        $urlMenuAtivoReestruturado='ativo/porTipoReestruturado'; $tituloMenuAtivoReestruturado='Reestruturado';
        $urlMenuAtivoInativo='ativo/porTipoInativo'; $tituloMenuAtivoInativo='Inativo';
        $urlMenuCarteira='ativoCarteiraBradesco/'; $tituloMenuCarteira='Carteira Bradesco';
        $urlAtivosProvisionados='ativoHistoricoProvisao/listaAtivosProvisionados'; $tituloAtivosProvisionados='Ativos provisionados';
        $urlHistoricoProvisionados='ativoHistoricoProvisao/historicoAtivosProvisionados'; $tituloHistoticoProvisionados='Histórico provisionados';
        $urlHistoricoValorAtualizado='ativoCarteiraBradesco/historicoValoresAtualizados'; $tituloValorAtualizado='Histórico valor atualizado';
        ?>
        <div class="accordion-group">
            <div class="accordion-heading">
                <a href="#collapseAtivos" data-parent="#menuPrincipal" data-toggle="collapse" class="accordion-toggle">
                    <i class="icon-font"></i> Ativos
                </a>
            </div>
            <div class="accordion-body collapse" id="collapseAtivos">
                <div class="accordion-inner">
                    <ul class="nav nav-pills nav-tabs nav-stacked">
                        <?php if($this->sessao->temPermissao(72,'ver')):?>
                            <?php $urlMenu=$urlMenuAtivo; $tituloMenu=$tituloMenuAtivo;?>
                            <li <?php if($tituloMenu == $urlMenu) echo ' class="active"' ?>>
                                <a href='<?php echo site_url($urlMenu)?>'>
                                    <?php echo $tituloMenu;?>
                                </a>
                            </li>
                            
                            <?php $urlMenu=$urlMenuAtivoReestruturado; $tituloMenu=$tituloMenuAtivoReestruturado;?>
                            <li <?php if($tituloMenu == $urlMenu) echo ' class="active"' ?>>
                                <a href='<?php echo site_url($urlMenu)?>'>
                                    <?php echo $tituloMenu;?>
                                </a>
                            </li>
                            
                            <?php $urlMenu=$urlMenuAtivoInativo; $tituloMenu=$tituloMenuAtivoInativo;?>
                            <li <?php if($tituloMenu == $urlMenu) echo ' class="active"' ?>>
                                <a href='<?php echo site_url($urlMenu)?>'>
                                    <?php echo $tituloMenu;?>
                                </a>
                            </li>
                            
                            <?php $urlMenu=$urlMenuCarteira; $tituloMenu=$tituloMenuCarteira;?>
                            <li <?php if($tituloMenu == $urlMenu) echo ' class="active"' ?>>
                                <a href='<?php echo site_url($urlMenu)?>'>
                                    <?php echo $tituloMenu;?>
                                </a>
                            </li>
                            
                            <?php $urlMenu=$urlAtivosProvisionados; $tituloMenu=$tituloAtivosProvisionados;?>
                            <li <?php if($tituloMenu == $urlMenu) echo ' class="active"' ?>>
                                <a href='<?php echo site_url($urlMenu)?>'>
                                    <?php echo $tituloMenu;?>
                                </a>
                            </li>
                            
                            <?php $urlMenu=$urlHistoricoProvisionados; $tituloMenu=$tituloHistoticoProvisionados;?>
                            <li <?php if($tituloMenu == $urlMenu) echo ' class="active"' ?>>
                                <a href='<?php echo site_url($urlMenu)?>'>
                                    <?php echo $tituloMenu;?>
                                </a>
                            </li>
                            
                            <?php $urlMenu=$urlHistoricoValorAtualizado; $tituloMenu=$tituloValorAtualizado;?>
                            <li <?php if($tituloMenu == $urlMenu) echo ' class="active"' ?>>
                                <a href='<?php echo site_url($urlMenu)?>'>
                                    <?php echo $tituloMenu;?>
                                </a>
                            </li>
                        <?php endif; ?>
                    </ul>
                </div>
            </div>
        </div>
    <?php endif; ?>
    
    <?php if($this->sessao->temPermissao(77,'ver')):?>
        <?php $urlMenu='garantia'; $tituloMenu='Garantias';?>
        <div class="accordion-group">
            <div class="accordion-heading">
                <a href="<?php echo site_url($urlMenu)?>" data-parent="#menuPrincipal" data-toggle="false" class="accordion-toggle">
                    <i class="icon-warning-sign"></i>
                    <?php echo $tituloMenu;?>
                </a>
            </div>
        </div>
    <?php endif; ?>
    
    <?php if($this->sessao->temPermissao(78,'ver')):?>
        <?php $urlMenu='pendencia'; $tituloMenu='Pendências';?>
        <div class="accordion-group">
            <div class="accordion-heading">
                <a href="<?php echo site_url($urlMenu)?>" data-parent="#menuPrincipal" data-toggle="false" class="accordion-toggle">
                    <i class="icon-tasks"></i>
                    <?php echo $tituloMenu;?>
                </a>
            </div>
        </div>
    <?php endif; ?>
    
    <?php if($this->sessao->temPermissao(277,'ver')):?>
        <?php $urlMenu='reuniao'; $tituloMenu='Reuniões';?>
        <div class="accordion-group">
            <div class="accordion-heading">
                <a href="<?php echo site_url($urlMenu)?>" data-parent="#menuPrincipal" data-toggle="false" class="accordion-toggle">
                    <i class="icon-calendar"></i>
                    <?php echo $tituloMenu;?>
                </a>
            </div>
        </div>
    <?php endif; ?>
    
    <?php if($this->sessao->temPermissao(345,'ver')):?>
        <?php $urlMenu='companhiaAlvo'; $tituloMenu='Companhia Alvo';?>
        <div class="accordion-group">
            <div class="accordion-heading">
                <a href="<?php echo site_url($urlMenu)?>" data-parent="#menuPrincipal" data-toggle="false" class="accordion-toggle">
                    <i class="icon-tags"></i>
                    <?php echo $tituloMenu;?>
                </a>
            </div>
        </div>
    <?php endif; ?>
    
    <!-- Relatórios -->
    <?php
    if($this->sessao->temPermissao(50691,'ver')):
        $urlRelatorioAtivo='relatorios/ativos'; $tituloRelatorioAtivo='Ativos';
        $urlRelatorioClassrating='relatorios/classificacaoRating'; $tituloRelatorioClassrating='Classificação de rating';
        $urlRelatorioEstruturaAdm='relatorios/estruturaAdm/Ativo'; $tituloRelatorioEstruturaAdm='Estrutura administrativa (Ativos)';
        $urlRelatorioEstruturaAdmAgente='relatorios/estruturaAdm/Agente'; $tituloRelatorioEstruturaAdmAgente='Estrutura administrativa (Agente)';
        $urlRelatoriorRecebimento='relatorios/recebimentos/'; $tituloRelatoriorecebimento='Recebimentos';
        $urlRelatoriorValorAtualizado='relatorios/valoresAtualizados'; $tituloRelatoriorValorAtualizado='Valores atualizados';
        $urlRelatoriorPendencia='relatorios/pendencias'; $tituloRelatoriorPendencia='Pendencias';
        $urlRelatoriorAssembleia='relatorios/assembleias'; $tituloRelatoriorAssembleia='Assembleias';
    ?>
        <div class="accordion-group">
            <div class="accordion-heading">
                <a href="#collapseRelatorios" data-parent="#menuPrincipal" data-toggle="collapse" class="accordion-toggle">
                    <i class="icon-file"></i> Relatórios
                </a>
            </div>
            <div class="accordion-body collapse" id="collapseRelatorios">
                <div class="accordion-inner">
                    <ul class="nav nav-pills nav-tabs nav-stacked">
                        <?php $urlMenu=$urlRelatorioAtivo; $tituloMenu=$tituloRelatorioAtivo; ?>
                        <li <?php if($tituloMenu == $urlMenu) echo ' class="active"'; ?>>
                            <a href='<?php echo site_url($urlMenu); ?>'>
                                <?php echo $tituloMenu; ?>
                            </a>
                        </li>                        
                        
                        <?php $urlMenu=$urlRelatoriorRecebimento; $tituloMenu=$tituloRelatoriorecebimento; ?>
                        <li <?php if($tituloMenu == $urlMenu) echo ' class="active"'; ?>>
                            <a href='<?php echo site_url($urlMenu); ?>'>
                                <?php echo $tituloMenu; ?>
                            </a>
                        </li>
                        
                        <?php $urlMenu=$urlRelatoriorValorAtualizado; $tituloMenu=$tituloRelatoriorValorAtualizado; ?>
                        <li <?php if($tituloMenu == $urlMenu) echo ' class="active"'; ?>>
                            <a href='<?php echo site_url($urlMenu); ?>'>
                                <?php echo $tituloMenu; ?>
                            </a>
                        </li>
                        
                        <?php $urlMenu=$urlRelatorioClassrating; $tituloMenu=$tituloRelatorioClassrating; ?>
                        <li <?php if($tituloMenu == $urlMenu) echo ' class="active"'; ?>>
                            <a href='<?php echo site_url($urlMenu); ?>'>
                                <?php echo $tituloMenu; ?>
                            </a>
                        </li>
                        
                        <?php $urlMenu=$urlRelatorioEstruturaAdm; $tituloMenu=$tituloRelatorioEstruturaAdm; ?>
                        <li <?php if($tituloMenu == $urlMenu) echo ' class="active"'; ?>>
                            <a href='<?php echo site_url($urlMenu); ?>'>
                                <?php echo $tituloMenu; ?>
                            </a>
                        </li>
                        
                        <?php $urlMenu=$urlRelatorioEstruturaAdmAgente; $tituloMenu=$tituloRelatorioEstruturaAdmAgente; ?>
                        <li <?php if($tituloMenu == $urlMenu) echo ' class="active"'; ?>>
                            <a href='<?php echo site_url($urlMenu); ?>'>
                                <?php echo $tituloMenu; ?>
                            </a>
                        </li>
                        
                        <?php $urlMenu=$urlRelatoriorPendencia; $tituloMenu=$tituloRelatoriorPendencia; ?>
                        <li <?php if($tituloMenu == $urlMenu) echo ' class="active"'; ?>>
                            <a href='<?php echo site_url($urlMenu); ?>'>
                                <?php echo $tituloMenu; ?>
                            </a>
                        </li>
<!--                        
                        <?php // $urlMenu=$urlRelatoriorAssembleia; $tituloMenu=$tituloRelatoriorAssembleia; ?>
                        <li <?php // if($tituloMenu == $urlMenu) echo ' class="active"'; ?>>
                            <a href='<?php // echo site_url($urlMenu); ?>'>
                                <?php // echo $tituloMenu; ?>
                            </a>
                        </li>-->
                    </ul>
                </div>
            </div>
        </div>
    <?php endif; ?>
    
    <?php if($this->sessao->temPermissao(72,'ver') || $this->sessao->temPermissao(73,'ver') || $this->sessao->temPermissao(74,'ver') 
            || $this->sessao->temPermissao(75,'ver')  || $this->sessao->temPermissao(79,'ver')  || $this->sessao->temPermissao(181,'ver') 
            || $this->sessao->temPermissao(267,'ver')) : ?>
        <div class="accordion-group">
            <div class="accordion-heading">
                <a href="#collapseTabela" data-parent="#menuPrincipal" data-toggle="collapse" class="accordion-toggle">
                    <i class="icon-list-alt"></i> Tabela
                </a>
            </div>
            <div class="accordion-body collapse" id="collapseTabela">
                <div class="accordion-inner">
                    <ul class="nav nav-pills nav-tabs nav-stacked">
                        <?php if($this->sessao->temPermissao(72,'ver')):?>
                            <?php $urlMenu='tipo'; $tituloMenu='Tipo de ativos';?>
                            <li <?php if($menuAtivo == $urlMenu) echo ' class="active"' ?>><a href='<?php echo site_url($urlMenu)?>'><?php echo $tituloMenu;?></a></li>
                        <?php endif; ?>

                        <?php if($this->sessao->temPermissao(73,'ver')):?>
                            <?php $urlMenu='segmento'; $tituloMenu='Segmentos';?>
                            <li id="segmentos" <?php if($menuAtivo == $urlMenu) echo ' class="active"' ?>><a href='<?php echo site_url($urlMenu)?>'><?php echo $tituloMenu;?></a></li>
                        <?php endif; ?>
                        
                        <?php if($this->sessao->temPermissao(74,'ver')):?>
                            <?php $urlMenu='agente'; $tituloMenu='Agentes';?>
                            <li <?php if($menuAtivo == $urlMenu) echo ' class="active"' ?>><a href='<?php echo site_url($urlMenu)?>'><?php echo $tituloMenu;?></a></li>
                        <?php endif; ?>

                        <?php if($this->sessao->temPermissao(75,'ver')):?>
                            <?php $urlMenu='evento'; $tituloMenu='Eventos';?>
                            <li id="eventos" <?php if($menuAtivo == $urlMenu) echo ' class="active"' ?>><a href='<?php echo site_url($urlMenu)?>'><?php echo $tituloMenu;?></a></li>
                        <?php endif; ?>
                        
                        <?php if($this->sessao->temPermissao(79,'ver')):?>
                            <?php $urlMenu='assembleia'; $tituloMenu='Assembleias';?>
                            <li <?php if($menuAtivo == $urlMenu) echo ' class="active"' ?>><a href='<?php echo site_url($urlMenu)?>'><?php echo $tituloMenu;?></a></li>
                        <?php endif; ?>
                            
                        <?php if($this->sessao->temPermissao(181,'ver')):?>
                            <?php $urlMenu='setor'; $tituloMenu='Atividade';?>
                            <li id="setores" <?php if($menuAtivo == $urlMenu) echo ' class="active"' ?>><a href='<?php echo site_url($urlMenu)?>'><?php echo $tituloMenu;?></a></li>
                        <?php endif; ?>
                            
                        <?php if($this->sessao->temPermissao(267,'ver')):?>
                            <?php $urlMenu='recebimento'; $tituloMenu='Recebimentos';?>
                            <li id="setores" <?php if($menuAtivo == $urlMenu) echo ' class="active"' ?>><a href='<?php echo site_url($urlMenu)?>'><?php echo $tituloMenu;?></a></li>
                        <?php endif; ?>
                            
                        <?php if($this->sessao->temPermissao(275,'ver')):?>
                            <?php $urlMenu='setor_reuniao'; $tituloMenu='Setor reuniões';?>
                            <li id="setores" <?php if($menuAtivo == $urlMenu) echo ' class="active"' ?>><a href='<?php echo site_url($urlMenu)?>'><?php echo $tituloMenu;?></a></li>
                        <?php endif; ?>
                    </ul>
                </div>
            </div>
        </div>
    <?php endif; ?>
    
    <?php if($this->session->userdata('usuario')): ?>
    <div class="accordion-group">
        <div class="accordion-heading">
            <a class="text-error accordion-toggle" data-parent="#myAccordion" href="<?php echo base_url('autenticacao/sair'); ?>">
                <i class="icon-signout icon-large"></i> SAIR</a>
        </div>
    </div>
    <?php endif; ?>
</div>