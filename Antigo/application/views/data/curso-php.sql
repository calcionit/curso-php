-- phpMyAdmin SQL Dump
-- version 4.3.4
-- http://www.phpmyadmin.net
--
-- Host: localhost
-- Tempo de geração: 03/06/2015 às 16:46
-- Versão do servidor: 5.5.39-MariaDB
-- Versão do PHP: 5.5.20

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Banco de dados: `curso-php`
--

-- --------------------------------------------------------

--
-- Estrutura para tabela `capitulos`
--

CREATE TABLE IF NOT EXISTS `capitulos` (
  `idCapitulo` int(11) NOT NULL,
  `idCurso` int(11) NOT NULL,
  `titulo` varchar(100) NOT NULL,
  `resumo` varchar(1000) DEFAULT NULL,
  `conteudo` text NOT NULL,
  `conclusao` varchar(1000) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Estrutura para tabela `codigos`
--

CREATE TABLE IF NOT EXISTS `codigos` (
  `idCodigo` int(11) NOT NULL,
  `idCapitulo` int(11) NOT NULL,
  `codigo` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Estrutura para tabela `cursos`
--

CREATE TABLE IF NOT EXISTS `cursos` (
  `idCurso` int(11) NOT NULL,
  `nome` varchar(80) DEFAULT NULL,
  `descicao` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Índices de tabelas apagadas
--

--
-- Índices de tabela `capitulos`
--
ALTER TABLE `capitulos`
  ADD PRIMARY KEY (`idCapitulo`), ADD KEY `idCurso` (`idCurso`);

--
-- Índices de tabela `codigos`
--
ALTER TABLE `codigos`
  ADD PRIMARY KEY (`idCodigo`), ADD KEY `idCapitulo` (`idCapitulo`);

--
-- Índices de tabela `cursos`
--
ALTER TABLE `cursos`
  ADD PRIMARY KEY (`idCurso`);

--
-- AUTO_INCREMENT de tabelas apagadas
--

--
-- AUTO_INCREMENT de tabela `capitulos`
--
ALTER TABLE `capitulos`
  MODIFY `idCapitulo` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT de tabela `codigos`
--
ALTER TABLE `codigos`
  MODIFY `idCodigo` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT de tabela `cursos`
--
ALTER TABLE `cursos`
  MODIFY `idCurso` int(11) NOT NULL AUTO_INCREMENT;
--
-- Restrições para dumps de tabelas
--

--
-- Restrições para tabelas `capitulos`
--
ALTER TABLE `capitulos`
ADD CONSTRAINT `capitulos_ibfk_1` FOREIGN KEY (`idCurso`) REFERENCES `cursos` (`idCurso`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Restrições para tabelas `codigos`
--
ALTER TABLE `codigos`
ADD CONSTRAINT `codigos_ibfk_1` FOREIGN KEY (`idCapitulo`) REFERENCES `capitulos` (`idCapitulo`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Restrições para tabelas `cursos`
--
ALTER TABLE `cursos`
ADD CONSTRAINT `cursos_ibfk_1` FOREIGN KEY (`idCurso`) REFERENCES `capitulos` (`idCurso`) ON DELETE CASCADE ON UPDATE CASCADE;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
