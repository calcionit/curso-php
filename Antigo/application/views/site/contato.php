<div>
    <h2 class="text-muted text-center"><?= $titulo; ?></h2>
    <hr />
    <p>
        Se você quiser um acompanhamento no curso para tirar eventuais dúvidas entre em contato
        comigo.
    </p>
    <p>O valor da consultoria é de <strong>R$ 750,00</strong> para o curso escolhido.</p>

    <?php $attributes = array('class' => 'form-horizontal', 'id' => 'frm-contato'); ?>
    <?= form_open('index.php/contato/send', $attributes) ?>
        <div class="form-group">
            <label for="consultoria" class="col-sm-2 control-label">Deseja consultoria:</label>
            <div class="col-lg-2">
                <?= form_dropdown('consultoria', $consultoria, 'class="form-control"') ?>
            </div>
        </div>

        <div class="form-group">
            <label for="nome" class="col-sm-2 control-label">Nome:</label>
            <div class="col-lg-4">
                <?= form_input('nome', $nome, 'class="form-control" maxlength="50"') ?>
            </div>
        </div>

        <div class="form-group">
            <label for="email" class="col-sm-2 control-label">E-mail:</label>
            <div class="col-lg-4">
                <?= form_input('email', $email, 'class="form-control" maxlength="100"') ?>
            </div>
        </div>

        <div class="form-group">
            <label for="curso" class="col-sm-2 control-label">Curso:</label>
            <div class="col-lg-6">
                <?= form_dropdown('curso', $curso, 'class="form-control"') ?>
            </div>
        </div>

        <div class="form-group">
            <label for="mensagem" class="col-sm-2 control-label">Mensagem:</label>
            <div class="col-lg-4">
                <?= form_textarea('mensagem', $mensagem, 'class="form-control"') ?>
            </div>
        </div>
        <div class="form-group text-center">
            <button type="submit" class="btn btn-primary">Enviar contato</button>
        </div>
  </div>
    <?= form_close() ?>
</div>
