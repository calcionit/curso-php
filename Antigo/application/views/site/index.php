        <!-- Jumbotron -->
        <div class="jumbotron">
            <h1>Bem vindo ao site Curso PHP</h1>
            <p class="lead">
            Encontre aqui um material atualizado, exemplos e lista de exercícios. O curso que ofereço inicialmente é o PHP Básico, após finalizar esse
            material, iniciarei com o PHP Orientado a Objetos do Jeito Certo.
            </p>
            <p><?= anchor('index.php', 'PHP Básico', 'class="btn btn-lg btn-success" role="button"'); ?></p>
        </div>

        <!-- Example row of columns -->
        <div class="row">
            <div class="col-lg-4">
                <h2>Não roda em <strong>IE</strong>ca</h2>
                <p class="text-danger"><strong>IE</strong>ca não é browser, é um pseudo navegador, medíocre que não respeita os Padrões do W3C</p>
                <p>Se você usa esse lixo, me desculpe baixe um navegador de verdade. Nem tente acessar. Depois não diga que não avisei!</p>
                <p class="text-center">
                    <a href="https://www.mozilla.org/pt-BR/firefox/new/" target="_new" title="Download Firefox">
                        <img data-echo="<?php echo base_url('assets/images/site/logo-firefox.png'); ?>" 
                        width="80" /></a>
                    &nbsp;&nbsp;&nbsp;
                    <a href="https://www.google.com/chrome/browser/desktop/index.html" target="_new" title="Download Chrome">
                        <img data-echo="<?php echo base_url('assets/images/site/logo-chrome.png'); ?>" 
                        width="80" /></a>
                    &nbsp;&nbsp;&nbsp;
                    <a href="http://www.opera.com/pt-br" target="_new" title="Download Opera">
                        <img data-echo="<?php echo base_url('assets/images/site/logo-opera.png'); ?>"
                        width="80" /></a>
                </p>
            </div>
            <div class="col-lg-4">
                <h2>PHP Básico</h2>
                <p>Aprenda como desenvolver em PHP, seguindo alguns dos padrõres do 
                <a href="http://www.php-fig.org/" target="_new" title="PHP fig"><strong>php-fig</strong></a> e 
                <a href="http://br.phptherightway.com" target="_new" title="PHP The Right Way"><strong>PHP The Right Way</strong></a>. Veja e pratique
                os exemplos mostrado. Evite copiar e colar.
                </p><br /><br /><br /><br />
                <p><?= anchor('index.php', 'Iniciar curso   &raquo;', 'class="btn btn-primary" role="button"'); ?></p>
            </div>

            <div class="col-lg-4">

                <h2>PHP Orientado a Objetos</h2>
                <p>Esse será o próximo curso a ser abordado. Continuaremos a falar do
                <a href="http://www.php-fig.org/" target="_new" title="PHP fig"><strong>php-fig</strong></a> e 
                <a href="http://br.phptherightway.com" target="_new" title="PHP The Right Way"><strong>PHP The Right Way</strong></a>.
                Seja paciente!</p>                
                <br /><br /><br /><br />
                <p><?= anchor('index.php/home/sobre', 'Mais detalhes   &raquo;', 'class="btn btn-primary" role="button"'); ?></p>
            </div>
        </div>