<div>
    <h2 class="text-muted text-center"><?= $titulo; ?></h2>
    <hr />

    <h3>Presente</h3>
    <p>
        O site tem como objetivo ajudar os novos desenvolvedores PHP a aprender de uma forma simples e correta.
        Abordarei a princípio o PHP básico, desde a sintaxe inicial até a contrução de uma classe simples.
        No final desse conteúdo terá um site feito em PHP com acesso a banco de dados, utilizando sessões, 
        autenticação e etc. Abordarei também um pouco do 
        <a href="http://www.php-fig.org/" target="_new" title="PHP fig"><strong>php-fig</strong></a> e o 
        <a href="http://br.phptherightway.com" target="_new" title="PHP The Right Way"><strong>PHP The Right Way</strong></a>
        HTML 5, CSS 3 usando o Twitter Bootstrap e o jQuery. Vale lembrar que o foco do curso é PHP.
    </p>

    <h3>Futuro</h3>
    <p>
        Após concluir o curso básico, pretendo criar um curso de PHP Orientado a Objetos, onde será explorado
        alguns dos conceitos da OO (Orientação a Objetos). Aqui aprenderá também um pouco mais sobre o 
        <a href="http://www.php-fig.org/" target="_new" title="PHP fig"><strong>php-fig</strong></a> e o 
        <a href="http://br.phptherightway.com" target="_new" title="PHP The Right Way"><strong>PHP The Right Way</strong></a>.
        E mais para frente pretendo montar um curso abordando um ou dois frameworks, mas isso depende 
        também do meu tempo.
    </p>

    <h3>Porque não em vídeo?</h3>
    <p>
        Infelizmente moro em uma casa de esquina com uma estrada que passa carro, caminhão e retardados
        com motos sem o cano de descarga e na outra rua tem uma igreja evangélica onde nos cultos parecem
        que estão sendo empalados tamanha é a gritaria e zona. Por esses motivos fica inviável eu gravar
        vídeos.
    </p>
</div>