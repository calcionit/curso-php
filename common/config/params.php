<?php
return [
    'adminEmail' => 'calcionit@gmail.com',
    'supportEmail' => 'calcionit@gmail.com',
    'user.passwordResetTokenExpire' => 3600,
];
